package edu.dnu.fpm.pz.groups.service;

import edu.dnu.fpm.pz.groups.model.Speciality;
import edu.dnu.fpm.pz.groups.repository.SpecialityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SpecialityService {
    private final SpecialityRepository repository;

    public void saveSpeciality(Speciality speciality) {
        repository.save(speciality);
    }

    public Speciality findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    public List<Speciality> listSpecialities() {
        return repository.findAll();
    }

    public void deleteSpeciality(Integer id) {
        repository.deleteById(id);
    }
}
