package edu.dnu.fpm.pz.groups.repository;

import edu.dnu.fpm.pz.groups.model.Base;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRepository extends JpaRepository<Base, Integer> {
}
