package edu.dnu.fpm.pz.groups.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class AuthorizationController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
