package edu.dnu.fpm.pz.groups.utility;

import edu.dnu.fpm.pz.groups.model.Payment;
import edu.dnu.fpm.pz.groups.model.Student;
import edu.dnu.fpm.pz.groups.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class StudentParser {
    private final Pattern pattern = Pattern.compile("(.+?)\\s(.+?)\\s(.+?)\\s(\\d+?(\\.?|\\,?)\\d+?)\\s(Б|К)", Pattern.MULTILINE);
    private final PaymentRepository paymentRepository;

    public List<Student> readOutFile(MultipartFile file) {
        List<Student> students = new ArrayList<>();

        Matcher matcher = null;

        try {
            matcher = pattern.matcher(new String(file.getInputStream().readAllBytes(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();

            return null;
        }

        while (matcher.find()) {
            Payment payment = paymentRepository.getById(matcher.group(6).equals("К") ? 1 : 2);

            students.add(new Student(null, matcher.group(1), matcher.group(2),
                    matcher.group(3), Double.parseDouble(matcher.group(4)), payment, null));
        }

        return students;
    }
}
