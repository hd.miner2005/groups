package edu.dnu.fpm.pz.groups.controller;

import edu.dnu.fpm.pz.groups.model.Student;
import edu.dnu.fpm.pz.groups.repository.PaymentRepository;
import edu.dnu.fpm.pz.groups.service.GroupService;
import edu.dnu.fpm.pz.groups.service.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
@RequiredArgsConstructor
@Tag(name = "Students")
public class StudentController {
    private final StudentService studentService;
    private final GroupService groupService;
    private final PaymentRepository paymentRepository;

    @DeleteMapping("/group/{id}/student/{idS}")
    public String deleteStudent(@PathVariable Integer id, @PathVariable Long idS) {
        studentService.deleteStudent(idS);
        return "redirect:/group/" + id;
    }

    @PutMapping("/group/{id}/student/{idS}")
    public String updateSpeciality(@PathVariable Integer id, @PathVariable Long idS, @RequestParam String pay, Student student) {
        Student currentStudent = studentService.findById(idS);

        List<String> names = Arrays.stream(student.getName().split(" ")).toList();
        currentStudent.setSurname(names.get(0));
        currentStudent.setName(names.get(1));
        currentStudent.setPatronymic(names.get(2));
        currentStudent.setAvgMark(student.getAvgMark());

        if(pay.equals("К")){
            currentStudent.setPayment(paymentRepository.getById(1));
        } else {
            currentStudent.setPayment(paymentRepository.getById(2));
        }

        studentService.saveStudent(currentStudent);
        return "redirect:/group/" + id;
    }

    @PutMapping("/group/{id}/student/move")
    @Operation(summary = "Move student from one group to another")
    public String moveStudent(@PathVariable Integer id, @RequestParam Long idS,  @RequestParam Integer idG) {
        Student student = studentService.findById(idS);
        groupService.findById(idG).addStudentToGroup(student);
        studentService.saveStudent(student);
        return "redirect:/group/" + id;
    }
}