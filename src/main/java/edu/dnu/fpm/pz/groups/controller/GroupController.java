package edu.dnu.fpm.pz.groups.controller;

import edu.dnu.fpm.pz.groups.model.Group;
import edu.dnu.fpm.pz.groups.model.Student;
import edu.dnu.fpm.pz.groups.repository.BaseRepository;
import edu.dnu.fpm.pz.groups.service.GroupService;
import edu.dnu.fpm.pz.groups.service.SpecialityService;
import edu.dnu.fpm.pz.groups.utility.StudentParser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.util.List;

@Controller
@RequiredArgsConstructor
@Tag(name = "Groups")
public class GroupController {
    private final GroupService groupService;
    private final SpecialityService specialityService;
    private final BaseRepository baseRepository;
    private final StudentParser parser;

    @GetMapping("/")
    public String groups(Model model, Principal principal) {
        model.addAttribute("groups", groupService.listGroups());
        model.addAttribute("bases", baseRepository.findAll());
        model.addAttribute("specialities", specialityService.listSpecialities());
        model.addAttribute("user", principal);
        return "groups";
    }

    @GetMapping("/group/{id}")
    public String group(@PathVariable Integer id, Model model, Principal principal) {
        model.addAttribute("group", groupService.findById(id));
        model.addAttribute("groups", groupService.listGroups());
        model.addAttribute("user", principal);
        return "students";
    }

    @PostMapping("/create_groups")
    @Operation(summary = "Generate new groups")
    public String createGroups(@RequestParam MultipartFile file, @RequestParam Integer amount,
                               @RequestParam(required = false) Integer free, @RequestParam(required = false) Integer contract,
                               @RequestParam Integer specialityId, @RequestParam Integer baseId,
                               @RequestParam(required = false) Boolean mark) {

        List<Student> students = parser.readOutFile(file);

        groupService.createGroups(amount, free, contract, mark, baseId, specialityId, students);
        return "redirect:/";
    }

    @PutMapping("/group/{id}")
    public String updateGroup(@PathVariable Integer id, Group group) {
        Group currentGroup = groupService.findById(id);

        currentGroup.setName(group.getName());

        groupService.saveGroup(currentGroup);

        return "redirect:/group/" + id;
    }
}
