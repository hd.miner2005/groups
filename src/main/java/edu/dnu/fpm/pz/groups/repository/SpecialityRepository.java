package edu.dnu.fpm.pz.groups.repository;

import edu.dnu.fpm.pz.groups.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {
}
