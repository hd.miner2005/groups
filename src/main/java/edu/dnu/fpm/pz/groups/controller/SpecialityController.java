package edu.dnu.fpm.pz.groups.controller;

import edu.dnu.fpm.pz.groups.model.Speciality;
import edu.dnu.fpm.pz.groups.service.SpecialityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class SpecialityController {
    private final SpecialityService specialityService;

    @GetMapping("/specialities")
    public String specialities(Model model, Principal principal) {
        model.addAttribute("specialities", specialityService.listSpecialities());
        model.addAttribute("user", principal);
        return "specialities";
    }

    @GetMapping("/speciality")
    @ResponseBody
    public String speciality(@PathVariable Integer id) {
        return specialityService.findById(id).toString();
    }

    @PostMapping("/speciality")
    public String createSpeciality(Speciality speciality) {
        specialityService.saveSpeciality(speciality);
        return "redirect:/specialities";
    }

    @DeleteMapping("/speciality/{id}")
    public String deleteSpeciality(@PathVariable Integer id) {
        specialityService.deleteSpeciality(id);
        return "redirect:/specialities";
    }

    @PutMapping("/speciality/{id}")
    public String updateSpeciality(@PathVariable Integer id, Speciality speciality) {
        Speciality currentSpeciality = specialityService.findById(id);

        currentSpeciality.setName(speciality.getName());
        currentSpeciality.setProgram(speciality.getProgram());
        currentSpeciality.setShortName(speciality.getShortName());

        specialityService.saveSpeciality(currentSpeciality);
        return "redirect:/specialities";
    }
}
