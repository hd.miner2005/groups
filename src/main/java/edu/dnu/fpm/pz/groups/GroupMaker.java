package edu.dnu.fpm.pz.groups;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupMaker {

    public static void main(String[] args) {
        SpringApplication.run(GroupMaker.class, args);
    }
}
