package edu.dnu.fpm.pz.groups.service;

import edu.dnu.fpm.pz.groups.model.Student;
import edu.dnu.fpm.pz.groups.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository repository;

    public void saveStudent(Student student) {
        repository.save(student);
    }

    public Student findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Student> listStudents() {
        return repository.findAll();
    }

    public void deleteStudent(Long id) {
        repository.deleteById(id);
    }
}
