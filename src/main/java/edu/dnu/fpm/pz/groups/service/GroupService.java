package edu.dnu.fpm.pz.groups.service;

import edu.dnu.fpm.pz.groups.model.Base;
import edu.dnu.fpm.pz.groups.model.Group;
import edu.dnu.fpm.pz.groups.model.Speciality;
import edu.dnu.fpm.pz.groups.model.Student;
import edu.dnu.fpm.pz.groups.repository.BaseRepository;
import edu.dnu.fpm.pz.groups.repository.GroupRepository;
import edu.dnu.fpm.pz.groups.repository.SpecialityRepository;
import edu.dnu.fpm.pz.groups.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupService {
    private final GroupRepository repository;
    private final BaseRepository baseRepository;
    private final SpecialityRepository specialityRepository;
    private final StudentRepository studentRepository;

    public void saveGroup(Group group) {
        repository.save(group);
    }

    public Group findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    public List<Group> listGroups() {
        return repository.findAll();
    }

    public void deleteGroup(Integer id) {
        repository.deleteById(id);
    }

    public void createGroups(Integer amount, Integer free, Integer contract, Boolean mark, Integer baseId, Integer specialityId, List<Student> students) {
        if (students == null) return;

        Speciality speciality = specialityRepository.getById(specialityId);
        Base base = baseRepository.getById(baseId);

        List<Group> groups = new ArrayList<>();

        for (int i = 1; i <= amount; i++) {
            Group group = new Group();
            group.setName(speciality.getShortName() + "-" + (LocalDate.now().getYear() - 2000) + base.getShortName() + "-" + i);
            group.setBase(base);
            group.setSpeciality(speciality);
            groups.add(group);
        }

        if (amount != 1) {
            if (free == null && contract == null && mark == null) {
                int scale = students.size() / amount;
                for (int i = 0; i < amount; i++) {
                    for (int j = i * scale; j < students.size(); j++) {
                        groups.get(i).addStudentToGroup(students.get(j));
                    }
                }
            } else if (mark != null) {
                List<Student> sorted = students.stream().sorted(Comparator.comparing(Student::getAvgMark)).collect(Collectors.toList());

                while (sorted.size() > 0) {
                    for (int i = 0; i < amount; i++) {
                        for (int j = 0; j < amount; j++) {
                            if (sorted.size() == 0) break;
                            groups.get(i).addStudentToGroup(sorted.get(0));
                            sorted.remove(0);
                        }

                        for (int j = 0; j < amount; j++) {
                            if (sorted.size() == 0) break;
                            groups.get(i).addStudentToGroup(sorted.get(sorted.size() - 1));
                            sorted.remove(sorted.size() - 1);
                        }
                    }
                }
            } else if (free != null && contract != null) {
                List<Student> frees = students.stream().filter(student -> student.getPayment().getName().charAt(0) == 'Б').collect(Collectors.toList());
                List<Student> contracts = students.stream().filter(student -> student.getPayment().getName().charAt(0) == 'К').collect(Collectors.toList());

                while (contracts.size() > 0 || frees.size() > 0) {
                    for (int i = 0; i < amount; i++) {
                        for (int j = 0; j < free; j++) {
                            if (frees.size() == 0) break;
                            groups.get(i).addStudentToGroup(frees.get(0));
                            frees.remove(0);
                        }

                        for (int j = 0; j < contract; j++) {
                            if (contracts.size() == 0) break;
                            groups.get(i).addStudentToGroup(contracts.get(0));
                            contracts.remove(0);
                        }
                    }
                }
            } else {
                List<Student> frees = students.stream().filter(student -> student.getPayment().getName().charAt(0) == 'Б').sorted(Comparator.comparing(Student::getAvgMark)).collect(Collectors.toList());
                List<Student> contracts = students.stream().filter(student -> student.getPayment().getName().charAt(0) == 'К').sorted(Comparator.comparing(Student::getAvgMark)).collect(Collectors.toList());

                while (contracts.size() > 0 || frees.size() > 0) {
                    for (int i = 0; i < amount; i++) {
                        for (int j = 0; j < free; j++) {
                            if (frees.size() == 0) break;
                            groups.get(i).addStudentToGroup(frees.get(0));
                            frees.remove(0);
                        }

                        for (int j = 0; j < free; j++) {
                            if (frees.size() == 0) break;
                            groups.get(i).addStudentToGroup(frees.get(frees.size() - 1));
                            frees.remove(frees.size() - 1);
                        }

                        for (int j = 0; j < contract; j++) {
                            if (contracts.size() == 0) break;
                            groups.get(i).addStudentToGroup(contracts.get(0));
                            contracts.remove(0);
                        }

                        for (int j = 0; j < contract; j++) {
                            if (contracts.size() == 0) break;
                            groups.get(i).addStudentToGroup(contracts.get(contracts.size() - 1));
                            contracts.remove(contracts.size() - 1);
                        }
                    }
                }
            }
        }

        groups.forEach(group -> saveGroup(group));
        students.forEach(student -> studentRepository.save(student));
    }
}
